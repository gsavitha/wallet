import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Header, Icon, Modal } from 'semantic-ui-react';

const ModalForm = () => {
  const [open, setOpen] = React.useState(false);

  return ReactDOM.createPortal(
    <div>
      <Modal
        closeIcon
        open={open}
        trigger={<Button>Show Modal</Button>}
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
      >
        <Header icon="archive" content="Archive Old Messages" />
        <Modal.Content>Login</Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={() => setOpen(false)}>
            <Icon name="remove" />
            Admin Login
          </Button>
          <Button color="green" onClick={() => setOpen(false)}>
            <Icon name="checkmark" /> User Login
          </Button>
        </Modal.Actions>
      </Modal>
    </div>,
    document.querySelector('#modal')
  );
};

export default ModalForm;
