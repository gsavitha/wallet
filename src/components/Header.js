import React from 'react';
import { Segment } from 'semantic-ui-react';
import GoogleAuth from './GoogleAuth';
import ModalForm from './Modal';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div>
      <GoogleAuth />
    </div>
  );
};

export default Header;
