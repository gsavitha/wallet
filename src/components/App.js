import React from 'react';
import Header from './Header';
import { Router, Route, Switch } from 'react-router-dom';

import HotelList from './hotels/HotelList';
import HotelCreate from './hotels/HotelCreate';
import HotelDelete from './hotels/HotelDelete';
import HotelEdit from './hotels/HotelEdit';
import HotelShow from './hotels/HotelShow';
import history from '../history';

const App = () => {
  return (
    <div className="ui container">
      <div>
        <Router history={history}>
          <Header />
          <Switch>
            <Route path="/" exact component={HotelList} />
            <Route path="/hotels/new" exact component={HotelCreate} />
            <Route path="/hotels/edit/:id" exact component={HotelEdit} />
            <Route path="/hotels/delete/:id" exact component={HotelDelete} />
            <Route path="/hotels/:id" exact component={HotelShow} />
          </Switch>
        </Router>
      </div>
    </div>
  );
};

export default App;
