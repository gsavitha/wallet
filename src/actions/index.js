import hotels from '../api/hotels';
import {
  SIGN_IN,
  SIGN_OUT,
  CREATE_HOTEL,
  EDIT_HOTEL,
  FETCH_HOTEL,
  FETCH_HOTELS,
  DELETE_HOTEL,
} from './types.js';
export const signIn = (userId) => {
  return {
    type: SIGN_IN,
    payload: userId,
  };
};

export const signOut = () => {
  return {
    type: SIGN_OUT,
  };
};

export const createHotel = (formValues) => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const res = await hotels.post('/hotels', { ...formValues, userId });
  dispatch({ type: CREATE_HOTEL, payload: res.data });
};

export const fetchHotels = () => async (dispatch) => {
  const res = await hotels.get('/hotels');
  dispatch({ type: FETCH_HOTELS, payload: res.data });
};

export const fetchHotel = (id) => async (dispatch) => {
  const res = await hotels.get(`/hotels/${id}`);
  dispatch({ type: FETCH_HOTEL, payload: res.data });
};

export const editHotel = (id, formValues) => async (dispatch) => {
  const res = await hotels.patch(`/hotels/${id}`, formValues);
  dispatch({ type: EDIT_HOTEL, payload: res.data });
};

export const deleteHotel = (id) => async (dispatch) => {
  await hotels.delete(`/hotels/${id}`);
  dispatch({ type: DELETE_HOTEL, payload: id });
};
