export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const CREATE_HOTEL = 'CREATE_HOTEL';
export const EDIT_HOTEL = 'EDIT_HOTEL';
export const FETCH_HOTELS = 'FETCH_HOTELS';
export const FETCH_HOTEL = 'FETCH_HOTEL';
export const DELETE_HOTEL = 'DELETE_HOTEL';
