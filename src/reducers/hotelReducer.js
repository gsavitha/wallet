import _ from 'lodash';
import {
  CREATE_HOTEL,
  FETCH_HOTELS,
  FETCH_HOTEL,
  EDIT_HOTEL,
  DELETE_HOTEL,
} from '../actions/types';

const HotelReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_HOTEL:
      return { ...state, [action.payload.id]: action.payload };
    case EDIT_HOTEL:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_HOTEL:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_HOTELS:
      return { ...state, ..._.mapKeys(action.payload, 'id') };
    case DELETE_HOTEL:
      return _.omit(state, action.payload);
    default:
      return state;
  }
};

export default HotelReducer;
